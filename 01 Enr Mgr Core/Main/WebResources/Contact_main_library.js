function Form_onload()
{
//disable High Score field
Xrm.Page.ui.controls.get("a422_highscore").setDisabled(true);

//******Populate dropdown loojps for state and country with saved text values and dummy guid.
if (Xrm.Page.ui.getFormType() != 1){
if (Xrm.Page.getAttribute("address1_stateorprovince").getValue() !== null){
var txtState = Xrm.Page.getAttribute("address1_stateorprovince").getValue();
Xrm.Page.getAttribute("a422_rel_stateid").setValue([{id: "{00000000-0000-0000-0000-000000000000}", name: txtState, entityType: "a422_dom_state"}]);
}
if (Xrm.Page.getAttribute("address1_country").getValue() !== null){
var txtCountry = Xrm.Page.getAttribute("address1_country").getValue();
Xrm.Page.getAttribute("a422_rel_countryid").setValue([{id: "{00000000-0000-0000-0000-000000000000}", name: txtCountry, entityType: "a422_dom_country"}]);
}
}

//Set these fields to never submit as they do not save data
Xrm.Page.getAttribute("a422_rel_stateid").setSubmitMode("never");
Xrm.Page.getAttribute("a422_rel_countryid").setSubmitMode("never");

//Use switch statement to hide geomarket on quick create and create forms
switch (Xrm.Page.ui.getFormType())
{
 case 5:
// Hide Geomarket field on the quick create form.
// It needs to be on the form to calculate the Geomarket and avoid errors
Xrm.Page.ui.controls.get("a422_geomarket").setVisible(false);
break;

case 1:
// Hide Geomarket field on the create form.
// It needs to be on the form to calculate the Geomarket and avoid errors
Xrm.Page.ui.controls.get("a422_geomarket").setVisible(false);
break;
}

//******adds id to the child div that contains the person id for use with image now
document.getElementById('footer_a422_em_person_id_d').childNodes[0].setAttribute('id','ImagePersonID');
}

function Form_onsave()
{
//Set lookup fields to null as they do not need to store values
Xrm.Page.getAttribute("a422_rel_stateid").setValue(null);
Xrm.Page.getAttribute("a422_rel_countryid").setValue(null);
}

function lkState_Change()
{
//Create variables for state and country fields
var newState = Xrm.Page.getAttribute("a422_rel_stateid");
var txtState = Xrm.Page.getAttribute("address1_stateorprovince");

//Set State and Country text fields to selected lookup values if not null.  Otherwise set text fields to null.
if (newState != null && newState.getValue() != null)
{
txtState.setValue(newState.getValue()[0].name);
}
else
{
txtState.setValue(null);
}
}

function lkCountry_Change()
{
//Create variables for state and country fields
var newCountry = Xrm.Page.getAttribute("a422_rel_countryid");
var txtCountry = Xrm.Page.getAttribute("address1_country");

//Set State and Country text fields to selected lookup values if not null.  Otherwise set text fields to null.
if (newCountry != null && newCountry.getValue() != null)
{
txtCountry.setValue(newCountry.getValue()[0].name);
}
else
{
txtCountry.setValue(null);
}
}

function a422_ssn_onchange()
{

//******Check SSN for NULL
var SSN = Xrm.Page.data.entity.attributes.get("a422_ssn"); 
	SSNValue = SSN.getValue();

if (SSNValue != null)
{
//Strip out non-numeric characters
SSNValue = SSNValue.replace(/[^0-9]/g, '');

//Check length of SSN for 9 digits
var LenSSN = SSNValue.length;

if (LenSSN != 9)
{
//alert user if invalid SSN is entered and set focus on SSN field
alert("The SSN number must contain 9 numeric characters.  Please enter a valid SSN or leave the field blank.");
//SSN.SetFocus();
event.returnValue = false;
return false;
}
else
{
// Create new variable to store formatted SSN and save
var NewSSN = "";
          for ( var i = 0 ; i < LenSSN ; i++ )
          {
               if ( ( i == 2 ) || ( i == 4 ) )
               {
                    NewSSN = NewSSN + SSNValue.charAt(i) + "-";
               }else{
                    NewSSN = NewSSN + SSNValue.charAt(i);
               }
          }
SSN.setValue(NewSSN);
}
}

}
