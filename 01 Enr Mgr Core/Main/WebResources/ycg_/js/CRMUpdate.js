﻿function updateCRM(GridRowId, odataSetName) {
 
	//Display processing Icon while saving
 	be = "<img src='../icons/ProcessingIcon.gif' alt='Edit Row' />";
	jQuery('#list').jqGrid('setRowData',GridRowId,{act:be});
 
    jQuery.support.cors = true;
	
	
	var IDindex = 0;
	var dataID = '';
	var edits = jQuery('#list').jqGrid('getRowData',GridRowId);
	var crmRecord = new Object();

	for(var index in edits) {
		if (IDindex == 0)
		{
		dataID = edits[index];
		}
		else if (IDindex == 4 || IDindex == 5)
		{
			if (edits[index] == " " || edits[index] == "")
			{
				edits[index] = null;
			}
			//Set completion date to now if waived is true and no date is set
			if (IDindex == 5 && edits.A422_Waived == 'true' && edits.A422_Completion == null)
			{
				var d = new Date();
				var month = d.getMonth() + 1;
				var day = d.getDate();
				var year = d.getFullYear();
				edits[index] = month + "/" + day + "/" + year;
			}
				crmRecord[index] = edits[index];
		}
		IDindex++;
	}
	
 
	var jsonEntity = window.JSON.stringify(crmRecord);

                         // Get Server URL
	var serverUrl = Xrm.Page.context.getServerUrl();
	//alert(serverUrl);
	//The OData end-point
	var ODATA_ENDPOINT = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Asynchronous AJAX function to Update a CRM record using OData

	$.ajax({
	type: "POST",
	contentType: "application/json; charset=utf-8",
	datatype: "json",
	data: jsonEntity,
	url: ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + dataID + "')",
	 
	beforeSend: function (XMLHttpRequest) {
	 
	//Specifying this header ensures that the results will be returned as JSON.
	 
	XMLHttpRequest.setRequestHeader("Accept", "application/json");
	 
	//Specify the HTTP method MERGE to update just the changes you are submitting.
	 
	XMLHttpRequest.setRequestHeader("X-HTTP-Method", "MERGE");
	 
	},
	 
	success: function (data, textStatus, XmlHttpRequest) {
	 
		//alert("Updated successfully");
		//Display saved icon
		var be = "<img id='c"+dataID+"'' src='../icons/SavedIcon.png' alt='Edit Row' />";
		jQuery('#list').jqGrid('setRowData',dataID,{act:be});
		document.getElementById("cbDirty").checked=false;
		
	},
	 
	error: function (XmlHttpRequest, textStatus, errorThrown) {
	 
		alert("Update Failed.  Please try the operation again or contact support for assistance." + errorThrown);
	 
	}
 
});
 
}