var XMLHTTPSUCCESS = 200; 
var XMLHTTPREADY = 4;
 
function FetchUtil(sOrg,sServer) 
{ 
this.org = sOrg; 
this.server = sServer;
 
if (sOrg == null) { 
if (typeof(ORG_UNIQUE_NAME) != "undefined") { 
this.org = ORG_UNIQUE_NAME; 
} 
} 

if (sServer == null){ 
this.server = window.location.protocol + "//" + window.location.host; 
} 
} 

FetchUtil.prototype._ExecuteRequest = function(sXml, sMessage, fInternalCallback, fUserCallback) 
{ 
var xmlhttp = new XMLHttpRequest(); 
xmlhttp.open("POST", this.server + "/XRMServices/2011/Organization.svc/web",(fUserCallback!=null)); 
xmlhttp.setRequestHeader("Accept", "application/xml, text/xml, */*"); 
xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8"); 
xmlhttp.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute"); 

if (fUserCallback!=null) 
{ 
//asynchronous: register callback function, then send the request. 
var crmServiceObject = this; 
xmlhttp.onreadystatechange = function(){ fInternalCallback.call(crmServiceObject,xmlhttp,fUserCallback) }; 
xmlhttp.send(sXml); 
} 
else 
{ 
//synchronous: send request, then call the callback function directly 
xmlhttp.send(sXml); 
return fInternalCallback.call(this,xmlhttp,null); 
} 
} 

FetchUtil.prototype._HandleErrors = function(xmlhttp) 
{ 
/// <summary>(private) Handles xmlhttp errors</summary> 
if (xmlhttp.status != XMLHTTPSUCCESS) { 
var sError = "Error: " + xmlhttp.responseText + " " + xmlhttp.statusText; 
alert(sError); 
return true; 
} else { 
return false; 
} 
} 

FetchUtil.prototype.Fetch = function(sFetchXml, fCallback) 
{ 
/// <summary>Execute a FetchXml request. (result is the response XML)</summary> 
/// <param name="sFetchXml">fetchxml string</param> 
/// <param name="fCallback" optional="true" type="function">(Optional) Async callback function if specified. If left null, function is synchronous </param>
 

var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">"; 
request += "<s:Body>"; 

request += '<Execute xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">' + 
'<request i:type="b:RetrieveMultipleRequest" ' + 
' xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" ' + 
' xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' + 
'<b:Parameters xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">' + 
'<b:KeyValuePairOfstringanyType>' + 
'<c:key>Query</c:key>' + 
'<c:value i:type="b:FetchExpression">' + 
'<b:Query>';
 
request += CrmEncodeDecode.CrmXmlEncode(sFetchXml); 

request += '</b:Query>' + 
'</c:value>' + 
'</b:KeyValuePairOfstringanyType>' + 
'</b:Parameters>' + 
'<b:RequestId i:nil="true"/>' + 
'<b:RequestName>RetrieveMultiple</b:RequestName>' + 
'</request>' + 
'</Execute>'; 

request += '</s:Body></s:Envelope>'; 

return this._ExecuteRequest(request,"Fetch", this._FetchCallback, fCallback); 
} 

FetchUtil.prototype._FetchCallback = function(xmlhttp,callback) 
{ 
///<summary>(private) Fetch message callback.</summary> 
//xmlhttp must be completed 
if (xmlhttp.readyState != XMLHTTPREADY) 
{ 
return; 
} 

//check for server errors 
if (this._HandleErrors(xmlhttp)) 
{ 
return; 
} 

var xmlReturn = xmlhttp.responseXML; 
//xmlReturn = xmlReturn.replace(/</g, '&lt;'); 
//xmlReturn = xmlReturn.replace(/>/g, '&gt;'); 

results = xmlReturn; 

//return entity id if sync, or call user callback func if async 
if (callback != null) 
{ 
callback(results); 
} 
else 
{ 
return results; 
} 
}
