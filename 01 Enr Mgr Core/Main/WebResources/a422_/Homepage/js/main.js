﻿function loadQNames()
	{
		//Load queries into dropdown lists
		getSavedQueries("savedquery");
		getSavedQueries("userquery");
	}

function getSavedQueries(qtype)
{
	//Set up the fecth service
	var _oService; 
	var _sOrgName = GetGlobalContext().getOrgUniqueName();
	var _sServerUrl = GetGlobalContext().getServerUrl();
	
	//Build two different fetch statements, one for saved queries and one for user queries
	if (qtype == "savedquery")
		{
		//Build the fetch statement
		var sFetch = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
		  "<entity name='savedquery'>" +
		    "<attribute name='name' />" +
		    "<attribute name='returnedtypecode' />" +
		    "<attribute name='querytype' />" +
		    "<order attribute='name' descending='false' />" +
		      "<filter type='and'>" +
		        "<filter type='or'>" +
					"<condition attribute='querytype' operator='eq' value='0' />" +
					"<condition attribute='querytype' operator='eq' value='1' />" +
					"<condition attribute='querytype' operator='eq' value='4' />" +
		        "</filter>" +
				"<filter type='or'>" +
					"<condition attribute='returnedtypecode' operator='eq' value='1' />" +
					"<condition attribute='returnedtypecode' operator='eq' value='2' />" +
					"<condition attribute='returnedtypecode' operator='eq' value='3' />" +
				"</filter>" +
		      "</filter>" +
		  "</entity>" +
		  "</fetch>";
		  }
	  else if (qtype == "userquery")
		  {
		  	//Build the fetch statement
			var sFetch = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
		  "<entity name='userquery'>" +
		    "<attribute name='name' />" +
		    "<attribute name='returnedtypecode' />" +
		    "<attribute name='querytype' />" +
		    "<order attribute='name' descending='false' />" +
		      "<filter type='and'>" +
		        "<filter type='or'>" +
					"<condition attribute='querytype' operator='eq' value='0' />" +
		        "</filter>" +
				"<filter type='or'>" +
					"<condition attribute='returnedtypecode' operator='eq' value='1' />" +
					"<condition attribute='returnedtypecode' operator='eq' value='2' />" +
					"<condition attribute='returnedtypecode' operator='eq' value='3' />" +
				"</filter>" +
		      "</filter>" +
		  "</entity>" +
		  "</fetch>";
		  }
	  else
	  	{
	  	alert("Invalid query type supplied");
	  	}

	
	//Execute fecth call (FetchUtil.js) and call myCallBack funtion on success
	_oService = new FetchUtil(_sOrgName, _sServerUrl); 
	var oEntity = _oService.Fetch(sFetch, myCallBack); 


	function myCallBack(results){ 
		
		//Declare variables for parsed results
		var records = results.getElementsByTagName("a:Entity");
		var rAtRoot;
		var rName;
		var rType;
		var rId;
		var eName;

		
		//loop through returned views and add them to the correct dropdown based on query type
		for (var i = 0; i < records.length; i++){ 
			rAtRoot = records[i].getElementsByTagName("a:Attributes")[0].getElementsByTagName("a:KeyValuePairOfstringanyType");
			rName = rAtRoot[0].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			rType = rAtRoot[1].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			rId = rAtRoot[3].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			eName = records[i].getElementsByTagName("a:LogicalName")[0].childNodes[0].nodeValue;
						
			switch(rType)
			 {
			 case "contact":
			 	addOptions(document.getElementById("ddlPersons"), rName, eName)
			   	break;
			 case "account":
			  	addOptions(document.getElementById("ddlAccounts"), rName, eName)
			   	break;
			 case "opportunity":
			   	addOptions(document.getElementById("ddlOpps"), rName, eName)
			   	break;
			 }
		} 
	}
}

function addOptions(ddl, label, value)
{
	var optn = document.createElement("OPTION");
    optn.text = label;
    optn.value = value;
    ddl.options.add(optn);
}

//gets the query info using selected dropdown or clicked default view (pad icon).
function getSelQuery(ddl)
{
	//set up variables
	var myddl = document.getElementById(ddl);
	var qName
	var qType
	var qEntity
	var qTypeCode
			
	//set query name, type, and type code based on passed in parameter
	switch(ddl)
	 {
	 case "ddlPersons":
	 	qEntity = "contact";
	 	qName = myddl.options[myddl.selectedIndex].text;
		qType = myddl.options[myddl.selectedIndex].value;
		qTypeCode = 2;
	   	break;
	 case "ddlAccounts":
	  	qEntity = "account";
	  	qName = myddl.options[myddl.selectedIndex].text;
		qType = myddl.options[myddl.selectedIndex].value;
		qTypeCode = 1;
	   	break;
	 case "ddlOpps":
	   	qEntity = "opportunity";
	   	qName = myddl.options[myddl.selectedIndex].text;
		qType = myddl.options[myddl.selectedIndex].value;
		qTypeCode = 3;
	   	break;
	 case "padPersons":
	   	qEntity = "contact";
	   	qName = 'Active Persons';
		qType = 'savedquery';
		qTypeCode = 2;
	   	break;
	 case "padAccounts":
	   	qEntity = "account";
	   	qName = 'Active Organizations';
		qType = 'savedquery';
		qTypeCode = 1;
	   	break;
	 case "padOpps":
	   	qEntity = "opportunity";
	   	qName = 'My Open Opportunities';
		qType = 'savedquery';
		qTypeCode = 3;
	   	break;
	 }
	 
	 //set value of hidden entity field on form
	 var txtEntity = document.getElementById('entityName');
	 txtEntity.value = qEntity;
	
	if (qType != '' && qType != undefined)
	{

		//Set up the fecth service
		var _oService; 
		var _sOrgName = GetGlobalContext().getOrgUniqueName();
		var _sServerUrl = GetGlobalContext().getServerUrl();
		
		  	//Build the fetch statement
			var sFetch = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
		  "<entity name='" + qType+ "'>" +
		    "<attribute name='name' />" +
		    "<attribute name='returnedtypecode' />" +
		    "<attribute name='querytype' />" +
		    "<attribute name='fetchxml' />" +
		    "<attribute name='layoutxml' />" +
		      "<filter type='and'>" +
					"<condition attribute='name' operator='eq' value='" + qName + "' />" +
					"<condition attribute='returnedtypecode' operator='eq' value='" + qTypeCode + "' />" +
		      "</filter>" +
		  "</entity>" +
		  "</fetch>";
		
		
		//Execute fecth call (FetchUtil.js) and call myCallBack funtion on success
		_oService = new FetchUtil(_sOrgName, _sServerUrl); 
		var oEntity = _oService.Fetch(sFetch, myCallBack);
	
	}
	else
	{
		alert('Please Select a valid view');
	}
	
	function myCallBack(results){ 
		//setup variables
		var records = results.getElementsByTagName("a:Entity")[0].getElementsByTagName("a:Attributes")[0].getElementsByTagName("a:KeyValuePairOfstringanyType");
		var qFetch = "<fetch></fetch>";
		var qLayout = "<grid></grid>";
		var r;
		
		//Loop through key value pairs to set form fields for form submit
		for (var i = 0; i < records.length; i++){
			
			r = records[i].getElementsByTagName("b:key")[0].childNodes[0].nodeValue;
			
			if (r == 'fetchxml')
			{
				var qFetch = records[i].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			}
			else if (r== 'layoutxml')
			{
				var qLayout = records[i].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			}
			else if (r == 'savedqueryid' || r == 'userqueryid')
			{
				var qID = records[i].getElementsByTagName("b:value")[0].childNodes[0].nodeValue;
			}
		}
				
		document.getElementById('fetchXml').value = qFetch;
		document.getElementById('layoutXml').value = qLayout;
		document.getElementById('defaultAdvFindViewId').value = qID;
		document.getElementById('viewId').value = qID;
		
		//submit the form passing query data to CRM result page
		var form = document.getElementById('resultRender');
		form.submit();
	}
}