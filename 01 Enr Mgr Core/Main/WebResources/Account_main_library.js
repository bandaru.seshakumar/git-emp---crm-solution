function IFRAME_OrgMap_onload()
{
 
}
function Form_onload()
{

//Call functions to change first State and Country fields to lookups and set lookup values from text fields if needed
//txtTolkState("address1_stateorprovince");
//txtTolkCountry("address1_country");
//var arg = document.getElementById('a422_rel_stateid_d').innerHTML;
//document.getElementById('a422_rel_stateid_d').innerHTML = arg;

//******Populate dropdown loojps for state and country with saved text values and dummy guid.
if (Xrm.Page.ui.getFormType() != 1){
if (Xrm.Page.getAttribute("address1_stateorprovince").getValue() !== null){
var txtState = Xrm.Page.getAttribute("address1_stateorprovince").getValue();
Xrm.Page.getAttribute("a422_rel_stateid").setValue([{id: "{00000000-0000-0000-0000-000000000000}", name: txtState, entityType: "a422_dom_state"}]);
}
if (Xrm.Page.getAttribute("address1_country").getValue() !== null){
var txtCountry = Xrm.Page.getAttribute("address1_country").getValue();
Xrm.Page.getAttribute("a422_rel_countryid").setValue([{id: "{00000000-0000-0000-0000-000000000000}", name: txtCountry, entityType: "a422_dom_country"}]);
}
}

//Set these fields to never submit as they do not save data
Xrm.Page.getAttribute("a422_rel_stateid").setSubmitMode("never");
Xrm.Page.getAttribute("a422_rel_countryid").setSubmitMode("never");

//Use switch statement to hide geomarket on quick create and create forms
switch (Xrm.Page.ui.getFormType())
{
 case 5:
// Hide Geomarket field on the quick create form.
// It needs to be on the form to calculate the Geomarket and avoid errors
Xrm.Page.ui.controls.get("a422_geomarket").setVisible(false);
break;

case 1:
// Hide Geomarket field on the create form.
// It needs to be on the form to calculate the Geomarket and avoid errors
Xrm.Page.ui.controls.get("a422_geomarket").setVisible(false);
break;
}
}

function Form_onsave()
{
//Set lookup fields to null as they do not need to store values
Xrm.Page.getAttribute("a422_rel_stateid").setValue(null);
Xrm.Page.getAttribute("a422_rel_countryid").setValue(null);
}

function lkState_Change()
{
//Create variables for state and country fields
var newState = Xrm.Page.getAttribute("a422_rel_stateid");
var txtState = Xrm.Page.getAttribute("address1_stateorprovince");

//Set State and Country text fields to selected lookup values if not null.  Otherwise set text fields to null.
if (newState != null && newState.getValue() != null)
{
txtState.setValue(newState.getValue()[0].name);
}
else
{
txtState.setValue(null);
}
}

function lkCountry_Change()
{
//Create variables for state and country fields
var newCountry = Xrm.Page.getAttribute("a422_rel_countryid");
var txtCountry = Xrm.Page.getAttribute("address1_country");

//Set State and Country text fields to selected lookup values if not null.  Otherwise set text fields to null.
if (newCountry != null && newCountry.getValue() != null)
{
txtCountry.setValue(newCountry.getValue()[0].name);
}
else
{
txtCountry.setValue(null);
}
}