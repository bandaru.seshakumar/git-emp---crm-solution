function Form_onload()
{
//******Only perform onload events if Domain field is true
if (crmForm.all.a422_domainyesno.DataValue == true)
{

//******Call javascript to build the metadata request to RetrieveAllEntities.

GetEntityList();
AddEntities(crmForm.all.a422_temp_domain, result);

//******Populate picklists and set selected values if this is an update form
if (crmForm.FormType != 1){

//******Call metadata webservice using global function to get all attributes for the selected entity, filter for custom attributes, and populate picklist options.
GetAttributeList(crmForm.all.a422_domainschema.DataValue.toLowerCase());
AddAttributes(crmForm.all.a422_temp_attribute, result);

attrValues(crmForm.all.a422_domainschema.DataValue.toLowerCase(),crmForm.all.a422_attributeschema.DataValue.toLowerCase());

//Call addOption function to populate the dropdown list
addOption(crmForm.all.a422_temp_crmname, crmForm.all.a422_attributeschema.DataValue.toLowerCase(), PrimaryKey,resultXml);

//******Populate dropdown list values with the stored text values if they exist.
//Then reset the default value of the of the dropdown fields to the newly assigned value to
//reset the IsDirty property.

SetPickText(crmForm.all.a422_temp_domain,crmForm.all.a422_domainschema);
SetPickText(crmForm.all.a422_temp_attribute,crmForm.all.a422_attributeschema);
SetPickText(crmForm.all.a422_temp_crmname,crmForm.all.a422_crmid);
}
}
else
{
//******If this is the create form hide the domain/attribute selection section
crmForm.all.a422_temp_domain_c.parentElement.parentElement.style.display='none';
crmForm.all.a422_temp_domain_c.parentElement.parentElement.parentElement.style.display='none';
}

}
function Form_onsave()
{
//******Ensure that a CRM Name and attribute have been selected if a Domain has been selected before saving.
if (crmForm.all.a422_domain.DataValue !== null &&
crmForm.all.a422_crmname.DataValue === null)
{
alert("If you have selected a Domain, you must also select a corresponding Attribute and CRM Name before saving the form.");
event.returnValue = false;
}

//******Set Dropdowns to null before saving to avoid errors related to 'unknown picklist values' 
//since CRM will not recognize the dynamically generated options.  Set Categories to null
//only if values have data.  This way if save is aborted due to validation above Categories remain.
if (crmForm.all.a422_domain.DataValue !== null &&
crmForm.all.a422_crmname.DataValue === null) 
{
}
else
{
crmForm.all.a422_temp_domain.DataValue = null;
crmForm.all.a422_temp_attribute.DataValue = null;
crmForm.all.a422_temp_crmname.DataValue  = null;
}
}
function a422_domainyesno_onchange()
{
//******Only perform onload events if Domain field is true
if (crmForm.all.a422_domainyesno.DataValue == true)
{

//******display domain/attribute selection section
crmForm.all.a422_temp_domain_c.parentElement.parentElement.style.display= '';
crmForm.all.a422_temp_domain_c.parentElement.parentElement.parentElement.style.display= '';

//******Call javascript to build the metadata request to RetrieveAllEntities.
GetEntityList();
AddEntities(crmForm.all.a422_temp_domain, result);

//******Populate picklists and set selected values if a domain schema name has been set
if (crmForm.all.a422_domainschema.DataValue != null){

//******Call metadata webservice using global function to get all attributes for the selected entity, filter for custom attributes, and populate picklist options.
GetAttributeList(crmForm.all.a422_domainschema.DataValue.toLowerCase());
AddAttributes(crmForm.all.a422_temp_attribute, result);

attrValues(crmForm.all.a422_domainschema.DataValue.toLowerCase(),crmForm.all.a422_attributeschema.DataValue.toLowerCase());

//Call addOption function to populate the dropdown list
addOption(crmForm.all.a422_temp_crmname, crmForm.all.a422_attributeschema.DataValue.toLowerCase(), PrimaryKey,resultXml);

//******Populate dropdown list values with the stored text values if they exist.
//Then reset the default value of the of the dropdown fields to the newly assigned value to
//reset the IsDirty property.

SetPickText(crmForm.all.a422_temp_domain,crmForm.all.a422_domainschema);
SetPickText(crmForm.all.a422_temp_attribute,crmForm.all.a422_attributeschema);
SetPickText(crmForm.all.a422_temp_crmname,crmForm.all.a422_crmid);
}

}
else
{
//******If not actual crm domain, hide the domain/attribute selection section
crmForm.all.a422_temp_domain_c.parentElement.parentElement.style.display='none';
crmForm.all.a422_temp_domain_c.parentElement.parentElement.parentElement.style.display='none';
}
}

function a422_temp_domain_onchange()
{
//******Remove all options from Attribute fields before adding new ones
RemoveOptions(crmForm.all.a422_temp_attribute,crmForm.all.a422_attribute);
crmForm.all.a422_attributeschema.DataValue = null;

//******Remove all options from CRM Name fields before adding new ones
RemoveOptions(crmForm.all.a422_temp_crmname,crmForm.all.a422_crmname);
crmForm.all.a422_crmid.DataValue = null;

// Set hidden fields to selected dropdown value
crmForm.all.a422_domain.DataValue = crmForm.all.a422_temp_domain.SelectedText;
crmForm.all.a422_domainschema.DataValue = crmForm.all.a422_temp_domain.DataValue;

//******Call metadata webservice using global function to get all attributes for the selected entity, filter for custom attributes, and populate picklist options.
GetAttributeList(DropDown.DataValue.toLowerCase());
AddAttributes(crmForm.all.a422_temp_attribute, result);
}
function a422_temp_attribute_onchange()
{
//Don't allow a null value to be selected
if (crmForm.all.a422_temp_attribute.DataValue == null)
{
alert("You must select an Attribute once you've selected a Domain");
}
else
{

//******Remove all options from CRM Name fields before adding new ones
RemoveOptions(crmForm.all.a422_temp_crmname,crmForm.all.a422_crmname);
crmForm.all.a422_crmid.DataValue = null;

//******Set Attribute text field to attribute name
crmForm.all.a422_attribute.DataValue = crmForm.all.a422_temp_attribute.SelectedText;
crmForm.all.a422_attributeschema.DataValue = crmForm.all.a422_temp_attribute.DataValue;


//******call webservices function to get dropdown options
var myDomain = crmForm.all.a422_temp_domain.DataValue;
var myAttribute = crmForm.all.a422_temp_attribute.DataValue;


attrValues(myDomain.toLowerCase(),myAttribute.toLowerCase());

//Call addOption function to populate the dropdown list
addOption(crmForm.all.a422_temp_crmname, myAttribute.toLowerCase(), PrimaryKey,resultXml);

}
}
function a422_temp_crmname_onchange()
{
//******Set CRM Name text field and ID field to selected CRM Name ID pair
crmForm.all.a422_crmname.DataValue = crmForm.all.a422_temp_crmname.SelectedText;
crmForm.all.a422_crmid.DataValue = crmForm.all.a422_temp_crmname.DataValue;
}