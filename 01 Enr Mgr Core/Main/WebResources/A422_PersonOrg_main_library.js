function Form_onload()
{
}

function a422_activeorg_onchange()
{
//Only perform this action if 'Yes' radio button has been selected

    var operson = Xrm.Page.getAttribute("a422_personid").getValue()[0].id;

    if (Xrm.Page.getAttribute("a422_activeorg").getValue() == true)
{
        var serverUrl = Xrm.Page.context.getServerUrl();
        var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
        //Query to get data for related activities
        var ODataQuery = ODataPath + "/A422_PersonOrgSet?$select=A422_Activeorg,a422_organizationid&$filter=(a422_personid/Id eq guid'" + operson + "') and (A422_Activeorg eq true)";
	
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            url: ODataQuery,
            beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
            success: function (data, textStatus, XmlHttpRequest)
            {
                var uJson = data.d.results;
                var oActive;
                var actorg;
				
                if (uJson.length > 0)
                {
                    actorg = uJson[0].a422_organizationid.Name;
                    alert("'" + actorg + "' is already marked as the active Organization for this Person.  Please set the Active field on that record to 'NO' before setting a new Active Organization.");
                    Xrm.Page.getAttribute("a422_activeorg").setValue(false);
                }	
            },
            error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Active Organization check failed: ' + ODataQuery); }
        });
}
}