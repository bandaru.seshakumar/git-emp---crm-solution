function IFRAME_upload_onload()
{
 
}
function Form_onload()
{
//******Extract the orgname from the url for use in script paths.  Adjust accordingly for custom
// client domains.
var query = window.location.href; 
var vars = query.split('/');
var varServer = vars[0] + "/" + "/" + vars[2];

//******Parse the Organization Soap Header to extract the organization name
var oSplit1 = oheader.split("CoreTypes\">");
//alert(oSplit1[3]);
var oSplit2 = oSplit1[3].split("<");
var varOrg = oSplit2[0];

//alert(varOrg);

//If create form diaplay Iframe.  Otherwise hide it.
if (Xrm.Page.ui.getFormType() == 1)
{
//Build Iframe path using retrieved varSever variable
Xrm.Page.getControl("IFRAME_upload").setSrc(varServer +"/ISV/webformscrm/FileUpload.aspx?orgname=" + varOrg);

Xrm.Page.ui.controls.get("a422_importname").setVisible(false);
Xrm.Page.ui.controls.get("a422_recordsuploaded").setVisible(false);
Xrm.Page.ui.controls.get("a422_sourcefile").setVisible(false);
Xrm.Page.ui.controls.get("createdby").setVisible(false);
Xrm.Page.ui.controls.get("createdon").setVisible(false);
Xrm.Page.ui.controls.get("a422_source").setVisible(false);
}
else
{
Xrm.Page.ui.controls.get("a422_importname").setDisabled(true);
Xrm.Page.ui.controls.get("a422_filetype").setDisabled(true);
Xrm.Page.ui.controls.get("a422_recordsuploaded").setDisabled(true);
Xrm.Page.ui.controls.get("a422_sourcefile").setDisabled(true);

Xrm.Page.ui.tabs.get("tab_3").setVisible(true);
Xrm.Page.ui.tabs.get("Import_Notes").setVisible(true);
Xrm.Page.ui.tabs.get("tab_4").setVisible(false);

}
}
function a422_importname_onchange()
{
child.document.forms[0].all.FiltType.setValue(Xrm.Page.getAttribute("a422_importname").getValue());
}
function a422_filetype_onchange()
{
try
{
	//Create variable for frame references
	var frameRef = "document.getElementById('IFRAME_upload').contentWindow.document";

	//Put the selected file type in a hidden Iframe field
	document.getElementById('IFRAME_upload').contentWindow.document.getElementById("FileType").value = Xrm.Page.getAttribute("a422_filetype").getSelectedOption().text;

	//disable browse and upload buttons until file type is selected
	document.getElementById('IFRAME_upload').contentWindow.document.getElementById("FileUpload1").disabled = false;

	document.getElementById('IFRAME_upload').contentWindow.document.getElementById("Upload").disabled = false;

	//Change Instructions to browse and upload
	document.getElementById('IFRAME_upload').contentWindow.document.getElementById("instructions").innerText = "Browse for and upload your file.";

	//Clear Div messages from Iframe
	document.getElementById('IFRAME_upload').contentWindow.document.getElementById("divMessage").innerHTML = "";
}
catch(err)
{
}
}