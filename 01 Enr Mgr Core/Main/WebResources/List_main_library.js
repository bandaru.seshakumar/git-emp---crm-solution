function IFRAME_export_onload()
{
 
}
function Form_onload()
{

//If activity category is null disable activity value
if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() == null)
    {
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
    }

//use Xrm Page context to get org name and user id
var c = Xrm.Page.context;
var varOrg = c.getOrgUniqueName();
var userid = c.getUserId();
var varServer = c.getClientUrl();
//alert(varOrg + "  " + userid);
	

//Build Iframe path using retrieved varSever variable
Xrm.Page.getControl("IFRAME_export").setSrc(varServer +"/ISV/WebformsCRM/outreach_form.aspx?type=4300&typename=list&id=" + Xrm.Page.data.entity.getId() + "&orgname=" + varOrg + "&userlcid=1033&orglcid=1033&userid=" + userid);

//******Hide Posting Trigger field
//crmForm.all.a422_postingtrigger_d.style.display = "none";

//******Replace Labels
document.getElementById("_NA_MA").innerHTML = 
document.getElementById("_NA_MA").innerHTML.replace("Marketing","Outreach");

//Replaces each instance of Marketing List Members with Outreach List Members
//the global command (g) was needed for submenu items.
document.getElementById("navListMember").innerHTML = 
document.getElementById("navListMember").innerHTML.replace(/Marketing List Members/g,"Outreach List Members");

switch (Xrm.Page.ui.getFormType())
{
 case 1:
Xrm.Page.getControl("IFRAME_export").setVisible(false);
 break;
}

//Removes Suspect from memeber type options.  This was added back in for 2011.
//document.getElementById("createdfromcode").remove(3);

}

function Form_onsave()
{

}

function a422_extendedmerge_onchange()
{
if (Xrm.Page.getAttribute("a422_extendedmerge").getValue() === true)
{
alert("Selecting this option will return data beyond the Person record including Opportunity information. This option should only be used with Outreach Lists containing less than 2,000 records. Selecting this option will cause the process to run more slowly and could take several minutes to complete. You must save the Outreach List with this option selected before running the mail merge.");
}
}

function Onchange_Activitycategory()
{
	if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() != null)
		{                                        
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(false);
		}
    else
        {                                       
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
        }
}