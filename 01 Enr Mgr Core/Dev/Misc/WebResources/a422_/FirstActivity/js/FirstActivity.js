function getActivities()
{
	//get list of activities and set dropdown to current first activity if not the create form
	if (window.parent.Xrm.Page.ui.getFormType() != 1)
	{
		var OppId = window.parent.Xrm.Page.data.entity.getId();
		var PersId = window.parent.Xrm.Page.data.entity.attributes.get("customerid").getValue()[0].id;
		
		//Call function to get activities for persons and opps and add them to the dropdown list
		OdataAct(OppId);
		OdataAct(PersId);
		
		var savedAct = window.parent.Xrm.Page.getAttribute("a422_firstactivity_guid").getValue();
		
		if (savedAct !== null)
		{
			savedAct = savedAct.toLowerCase();
			//set dropdown value if current hidden guid field is not null
			document.getElementById("lstFirstAct").value = savedAct;
		}
	}
}

function OdataAct(obj)
{
	var context = GetGlobalContext();
	var serverUrl = context.getServerUrl();
	var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Query to get data for related activities
	var ODataQuery = ODataPath + "/ActivityPointerSet?$select=ActivityId,ActualEnd,Subject&$filter=RegardingObjectId/Id eq guid'" + obj + "'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var oLabel;
				var oEndDate = new Date();
				var oDay;
				var oMonth;
				var oYear;
				var oDateString;
				
				//Loop through returned activities and add them to dropdown list
                for( i=0; i< uJson.length; i++)
				{
					//alert(uJson[i].ActivityId + "   " + uJson[i].Subject + " - " + uJson[i].ActualEnd);
					oEndDate = new Date(parseInt(uJson[i].ActualEnd.replace("/Date(", "").replace(")/", ""), 10));
					oMonth = oEndDate.getMonth() + 1;
					oDay = oEndDate.getDate();
					oYear = oEndDate.getFullYear();
					oDateString = oMonth + "/" + oDay + "/" + oYear;
					oLabel = uJson[i].Subject + " - " + oDateString;
					var optn = document.createElement("OPTION");
					optn.text = oLabel;
					optn.value = "{" + uJson[i].ActivityId + "}";
					document.getElementById('lstFirstAct').options.add(optn);	
				}

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Activity call failed: ' + ODataQuery); }
   });
}

function FirstActChange()
{
	//when selection is made lookup activitiy category and value for the activitiy and set the values of the hidden fields
	
	//first lookup activity type code in pointer table to figure out whether it is a task, email, etc.
	var context = GetGlobalContext();
	var serverUrl = context.getServerUrl();
	var obj = document.getElementById("lstFirstAct").value;
	var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Query to get data for Opportunity to be merged
	var ODataQuery = ODataPath + "/ActivityPointerSet?$select=ActivityTypeCode&$filter=ActivityId eq guid'" + obj + "'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var selActType = data.d.results[0].ActivityTypeCode;
				var capType = selActType.charAt(0).toUpperCase() + selActType.slice(1);
				var actSet = capType + "Set";
				
				//use activity type code to query proper entity to get activity category and value
				var ODataQuery = ODataPath + "/" + actSet + "?$select=ActivityId,a422_rel_activitycategoryid,a422_rel_activityvalueid,ActualEnd&$filter=ActivityId eq guid'" + obj + "'";
				
				$.ajax({
				   type: "GET",
				   contentType: "application/json; charset=utf-8",
				   datatype: "json",
				   async: false,
				   url: ODataQuery,
				   beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
				   success: function (data, textStatus, XmlHttpRequest)
					   {
							var uJson = data.d.results[0];
							var actGuid = "{" + uJson.ActivityId + "}";
							var actCat = uJson.a422_rel_activitycategoryid.Name;
							var actVal = uJson.a422_rel_activityvalueid.Name;
							var oEndDate = new Date(parseInt(uJson.ActualEnd.replace("/Date(", "").replace(")/", ""), 10));
							var oMonth = oEndDate.getMonth() + 1;
							var oDay = oEndDate.getDate();
							var oYear = oEndDate.getFullYear();
							var oDateString = oMonth + "/" + oDay + "/" + oYear;
							
							var firstActDesc = actCat + ": " + actVal + " - " + oDateString;
							// Set hidden fields to selected dropdown value

							window.parent.Xrm.Page.getAttribute("a422_firstactivity").setValue(firstActDesc);
							window.parent.Xrm.Page.getAttribute("a422_firstactivity_guid").setValue(actGuid);
					   },
				   error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Activity call failed: ' + ODataQuery); }
			   });
				
           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Activity call failed: ' + ODataQuery); }
   });
}