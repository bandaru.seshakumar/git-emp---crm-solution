function Form_onload()
{
	if (Xrm.Page.ui.getFormType() != 1)
	{
	    showDomAttSelect();
	}
}

function Form_onsave()
{

}

function a422_domainyesno_onchange()
{
showDomAttSelect();
}

function showDomAttSelect()
{
//Display Domain Selection section is domain yes no is set to true
if (Xrm.Page.getAttribute("a422_domainyesno").getValue() == true)
{
//******display domain/attribute selection section
Xrm.Page.ui.tabs.get("GeneralTab").sections.get("DomAttSelection").setVisible(true);
Xrm.Page.ui.tabs.get("GeneralTab").sections.get("DomValues").setVisible(true);
}
else
{
Xrm.Page.ui.tabs.get("GeneralTab").sections.get("DomAttSelection").setVisible(false);
Xrm.Page.ui.tabs.get("GeneralTab").sections.get("DomValues").setVisible(false);
}
}

function enableFields()
{
 var attributes = Xrm.Page.data.entity.attributes.get();
 for (var i in attributes)
 {
 Xrm.Page.getControl(attributes[i].getName()).setDisabled(false);
 }
}