function Form_onload()
{

//******Clear any mapped value from Advisor field
if (Xrm.Page.ui.getFormType() == 1)
{
Xrm.Page.getAttribute("a422_rel_advisorid").setValue(null);
}

//******Close window function called from Opp Merge
OppClose = function ()
{

    //Refreshes Opp grid view on person form if the opp was opened from the person and closes the merged opportunity
    
    try
    {
        window.top.opener.crmGrid_opportunity_customer_contacts.Refresh();

        //Closes the opp window
        Xrm.Page.ui.close();
    }
    catch(err)
    {
        //Closes the opp window
       Xrm.Page.ui.close();
    }

}

//******Hide First Activity Dropdown if it's the Create Form
if (Xrm.Page.ui.getFormType() == 1){
Xrm.Page.ui.tabs.get("tabProspectiveStudent").sections.get("secFirstActivity").setVisible(false);
}

//******Retrieves the PersonID from the parent person record for display in the opportunity footer
function GetPersID()
{
var lkPers = Xrm.Page.getAttribute("customerid").getValue();

    if (lkPers != null) 
		{
		
		var lkPersID = lkPers[0].id;
		
		try
			{
				var serverUrl = Xrm.Page.context.getServerUrl();
				var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
				//Query to get data for EM PersonID
				var ODataQuery = ODataPath + "/ContactSet?$select=a422_EM_Person_ID&$filter=ContactId eq guid'" + lkPersID + "'";
				
				$.ajax({
				   type: "GET",
				   contentType: "application/json; charset=utf-8",
				   datatype: "json",
				   async: false,
				   url: ODataQuery,
				   beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
				   success: function (data, textStatus, XmlHttpRequest)
				   {
						var uJson = data.d.results[0];
						var oPersonId = uJson.a422_EM_Person_ID;
						document.getElementById('footer_actualvalue_d').childNodes[0].innerHTML = oPersonId;
						//adds id to the child div that contains the person id for use with image now
						document.getElementById('footer_actualvalue_d').childNodes[0].setAttribute('id','ImagePersonID');
				   },
			   error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Activity call failed: ' + ODataQuery); }
			   });
			}
		catch(err)
			{
			return ""
			}
		}
}
//******Sets the value as a placeholder field in the footer to the Person ID
if (Xrm.Page.ui.getFormType() != 1){
GetPersID();
}
//adds id to the child div that contains the opportunity id for use with image now
document.getElementById('footer_a422_em_opportunity_id_d').childNodes[0].setAttribute('id','ImageOppID');
}

function Form_onsave(executionObj)
{
//******builds the value for the name attribute from the related person
//and the entry term for use in the window titles and views
var oppname = Xrm.Page.getAttribute("customerid").getValue()[0].name + " - " + Xrm.Page.getAttribute("a422_rel_entrytermid").getValue()[0].name;
Xrm.Page.getAttribute("name").setValue(oppname);
var gridCk = Xrm.Page.getControl("WebResource_requirements_grid").getObject().contentWindow.document.getElementById("cbDirty").checked;
if (gridCk == true)
    {
        var r=confirm("You may have unsaved changes in the requirement grid.  To proceed without saving possible changes click OK, otherwise click Cancel.");
        if (r==true)
        {
        }
        else
        {
            executionObj.getEventArgs().preventDefault();
        } 
    }
}

function gridAccess()
{
    if (!UserHasRole("EM - Area - ReqGrid")) 
        {
            //Xrm.Page.ui.tabs.get("TabReqGrid").setVisible(false);
            Xrm.Page.ui.tabs.get("TabReqGrid").sections.get("SecReqEditGrid").setVisible(false);
        }
        else
        {
            Xrm.Page.ui.tabs.get("TabReqGrid").sections.get("SecReqSubGrid").setVisible(false);
        }        
}