//Pass the role which you want to check, as Parameter (i.e.,System Administrator etc…)
 
// It fetch the role information which you passed as parameter using OData Service
 
function UserHasRole(roleName) {
 
var serverUrl = Xrm.Page.context.getServerUrl();
var oDataEndpointUrl = serverUrl + "/XRMServices/2011/OrganizationData.svc/";
oDataEndpointUrl += "RoleSet?$filter=Name eq '" + roleName + "'&$select=RoleId";
var service = GetRequestObject();
 
if (service != null) {
service.open("GET",oDataEndpointUrl, false);
service.setRequestHeader("X-Requested-Width", "XMLHttpRequest");
service.setRequestHeader("Accept", "application/json,text/javascript, */*");
service.send(null);
var requestResults = eval('(' +service.responseText + ')').d;
var currentUserRoles = Xrm.Page.context.getUserRoles();

 if (requestResults != null && requestResults.results.length > 0) {
 
for (var i = 0; i < requestResults.results.length; i++) {
 
var role = requestResults.results[i];
 var id = role.RoleId;
 
 for (var j = 0; j < currentUserRoles.length; j++) {
 var userRole = currentUserRoles[j];
 if (GuidsAreEqual(userRole, id)) {
 return true;
 }
 }
 }
 }
 }
 
return false;
 }
 
function GetRequestObject() {

if (window.XMLHttpRequest) {
return new window.XMLHttpRequest;
}
else {
try {
return new ActiveXObject("MSXML2.XMLHTTP.3.0");
}
catch (ex) {
return null;
}
}
}
 
function GuidsAreEqual(guid1, guid2) {
var isEqual = false;
if (guid1 == null || guid2 == null){
isEqual = false;
}
 
else {
isEqual = (guid1.replace(/[{}]/g, "").toLowerCase() == guid2.replace(/[{}]/g, "").toLowerCase());
}
return isEqual;
}

//Remove commas from a numeric display
function RemoveComma(fieldname)
{
document.getElementById(fieldname).value = Xrm.Page.data.entity.attributes.get(fieldname).getValue();
}

//Lookup and return Geomarket using state and zip
function getGeo(zip, state)
{
	var serverUrl = Xrm.Page.context.getServerUrl();
	var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Query to get data for related activities
	var ODataQuery = ODataPath + "/A422_ZipCodeSet?$select=a422_geomarketid&$filter=A422_actualzip eq '" + zip + "' and A422_zipState eq '" + state + "'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var geo = null;
				
				//Loop through returned activities and add them to dropdown list
                if (uJson.length > 0)
				{
					//alert(uJson[i].ActivityId + "   " + uJson[i].Subject + " - " + uJson[i].ActualEnd);
					oEndDate = new Date(parseInt(uJson[i].ActualEnd.replace("/Date(", "").replace(")/", ""), 10));
					geo = uJson[0].a422_geomarketid.Name;
				}
				
				return geo;

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Activity call failed: ' + ODataQuery); }
   });
}

function statePick(obj)
{
	var crmField = document.getElementById(obj + "_d");
	crmField.innerHTML = '<select id="emStatePick" class="ms-crm-SelectBox"><option value=""></option></select>'
	
	var serverUrl = Xrm.Page.context.getServerUrl();
	var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Query to get data for related activities
	var ODataQuery = ODataPath + "/A422_dom_stateSet?$select=A422_state&$orderby=A422_DisplayOrder";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var abrState;
				var pickField = document.getElementById('emStatePick');
				//Loop through returned activities and add them to dropdown list
                for( i=0; i< uJson.length; i++)
				{
					//alert(uJson[i].ActivityId + "   " + uJson[i].Subject + " - " + uJson[i].ActualEnd);
					abrState = uJson[i].A422_state;
					var optn = document.createElement("OPTION");
					optn.text = abrState;
					optn.value = abrState;
					pickField.options.add(optn);
					pickField.value = Xrm.Page.getAttribute(obj).getValue();
				}

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('State Domain call failed: ' + ODataQuery); }
   });
}

function countryPick(obj)
{
	var crmField = document.getElementById(obj + "_d");
	crmField.innerHTML = '<select id="emCountryPick" class="ms-crm-SelectBox"><option value=""></option></select>'
	
	var serverUrl = Xrm.Page.context.getServerUrl();
	var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
	//Query to get data for related activities
	var ODataQuery = ODataPath + "/A422_dom_countrySet?$select=A422_Country&$orderby=A422_DisplayOrder";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var emCountry;
				var pickField = document.getElementById('emCountryPick');
				//Loop through returned activities and add them to dropdown list
                for( i=0; i< uJson.length; i++)
				{
					//alert(uJson[i].ActivityId + "   " + uJson[i].Subject + " - " + uJson[i].ActualEnd);
					emCountry = uJson[i].A422_Country;
					var optn = document.createElement("OPTION");
					optn.text = emCountry;
					optn.value = emCountry;
					pickField.options.add(optn);
					pickField.value = Xrm.Page.getAttribute(obj).getValue();
				}

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('State Domain call failed: ' + ODataQuery); }
   });
}

//Sets value of State text field to lookup value on save
function saveAdress(txtField, pickId)
{
    var newVal = null;
	var pickField = document.getElementById(pickId).value;

	if (pickField !== null)
	{
	newVal = pickField;
	}
	Xrm.Page.getAttribute(txtField).setValue(newVal);
}