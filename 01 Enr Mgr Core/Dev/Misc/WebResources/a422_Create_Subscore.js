// JScript source code

function CreateSubscore() {

if (Xrm.Page.getAttribute("a422_rel_subscoretestnameid").getValue() == null || Xrm.Page.getAttribute("a422_subscorescore").getValue() == null)
{
	alert('Test Name and Score are required to create a Subscore');
}
else
{
    var Ename = Xrm.Page.data.entity.getEntityName();
    /*alert(Ename);*/
    var context = Xrm.Page.context;
    var serverUrl = context.getServerUrl();
    var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
    //var CRMObject = new Object(); 
    ///////////////////////////////////////////////////////////// 

    // Specify the ODATA entity collection 

    var ODATA_EntityCollection = "/A422_personscoreSet";

    ///////////////////////////////////////////////////////////// 

    var perc = Xrm.Page.getAttribute("a422_subscorepercentile").getValue();
    
    //Build record name for new subscore
    var operson = Xrm.Page.getAttribute("a422_rel_personid").getValue()[0].name;
    var otest = Xrm.Page.getAttribute("a422_rel_subscoretestnameid").getValue()[0].name;
    var orecname = operson + " - " + otest;

	
    // Define attribute values for the CRM object you want created
    var CRMObject = {
        A422_name: orecname,
        A422_compositeScore: Xrm.Page.getAttribute("a422_subscorescore").getValue().toString(),
        A422_a422_compositepercentile: perc,
        A422_TestDate: Xrm.Page.getAttribute("a422_testdate").getValue(),
        a422_rel_parentscoreid: {
            __metadata: { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" },
            Id: Xrm.Page.data.entity.getId(), // Guid of the parent Service Activity
            LogicalName: Ename
        },
        a422_rel_testnameid: {
            __metadata: { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" },
            Id: Xrm.Page.getAttribute("a422_rel_subscoretestnameid").getValue()[0].id, // Guid of the parent Service Activity
            LogicalName: 'a422_dom_testname'
        },
        a422_rel_scoresourceid: {
            __metadata: { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" },
            Id: Xrm.Page.getAttribute("a422_rel_scoresourceid").getValue()[0].id, // Guid of the parent Service Activity
            LogicalName: 'a422_dom_scoresource'
        },
        a422_rel_personid: {
            __metadata: { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" },
            Id: Xrm.Page.getAttribute("a422_rel_personid").getValue()[0].id, // Guid of the parent Service Activity
            LogicalName: 'contact'
        },
        a422_rel_ParentTestNameId: {
            __metadata: { type: "Microsoft.Crm.Sdk.Data.Services.EntityReference" },
            Id: Xrm.Page.getAttribute("a422_rel_testnameid").getValue()[0].id, // Guid of the parent Service Activity
            LogicalName: 'a422_dom_testname'
        }
    }
    //Parse the entity object into JSON 
    var jsonEntity = window.JSON.stringify(CRMObject);

    //Asynchronous AJAX function to Create a CRM record using OData
    $.ajax({ type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: serverUrl + ODATA_ENDPOINT + ODATA_EntityCollection,
        data: jsonEntity,
        beforeSend: function (XMLHttpRequest) {

            //Specifying this header ensures that the results will be returned as JSON. 
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            //alert("success");
            Xrm.Page.getAttribute("a422_subscorescore").setValue(null);
            Xrm.Page.getAttribute("a422_subscorepercentile").setValue(null);
            Xrm.Page.getAttribute("a422_rel_subscoretestnameid").setValue(null);
            gridControl = Xrm.Page.ui.controls.get("SubScores");
            gridControl.refresh();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Record Creation Failed!" + errorThrown);
        }
    });
}
}

function ClearSubscores(){
    Xrm.Page.getAttribute("a422_subscorescore").setValue(null);
    Xrm.Page.getAttribute("a422_subscorepercentile").setValue(null);
    Xrm.Page.getAttribute("a422_rel_subscoretestnameid").setValue(null);
}