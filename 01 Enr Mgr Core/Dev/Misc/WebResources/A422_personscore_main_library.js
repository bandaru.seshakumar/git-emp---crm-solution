function Form_onload()
{

//******Extract the orgname from the url for use in script paths.  Adjust accordingly for custom
// client domains.
var query = window.location.href; 
var vars = query.split('/');
var varServer = "https://" + vars[2];
if (vars[2] === "titan.422g.com"){
varServer = "https://" + vars[2];
}

//Add Create Subscore event to save web resource image
document.getElementById('WebResource_save_subscore').onclick = function() { CreateSubscore(); };
$("#WebResource_save_subscore").css('cursor', 'pointer');

//Hide Parent Score Lookup if it has no value and set test name to show only Parent Scores;
if (Xrm.Page.getAttribute("a422_rel_parentscoreid").getValue() === null){
//set test name lookup to parent score viw
Xrm.Page.getControl("a422_rel_testnameid").setDefaultView("D8F755F1-5DFB-E111-92BC-000C29382EBF");
Xrm.Page.ui.controls.get("a422_rel_parentscoreid").setVisible(false);
}

if (Xrm.Page.ui.getFormType() != 1 && Xrm.Page.getAttribute("a422_rel_parentscoreid").getValue() == null){
Xrm.Page.ui.tabs.get("AddSubscores").setVisible(true);
Xrm.Page.ui.tabs.get("SubscoreGrid").setVisible(true);
}

//Don't display subscore Iframe on create or quick create forms and set the value of the hidden name field to a temporary value until the record is saved.
if (Xrm.Page.ui.getFormType() == 1 || Xrm.Page.ui.getFormType() == 5 )
{
Xrm.Page.getAttribute("a422_name").setValue("Temp Score Name");
}

//******Disable 'Source' field on the update form if it has a value
if (Xrm.Page.getAttribute("a422_rel_scoresourceid").getValue() !== null){
Xrm.Page.ui.controls.get("a422_rel_scoresourceid").setDisabled(true);
document.getElementById('a422_rel_scoresourceid_d').onclick = function() {alert("The Score Source can not be modified once it has been saved.  Please create a new Score record with the updated Source.");};
}

//******Disable Parent Score Field
Xrm.Page.ui.controls.get("a422_rel_parentscoreid").setDisabled(true);

//temporarily sets the test record name to avoid onsave error and is reset during onsave event
if (Xrm.Page.getAttribute("a422_name").getValue() == null)
{
Xrm.Page.getAttribute("a422_name").setValue("Temp Name");
}

}

function Form_onsave()
{
var operson = Xrm.Page.getAttribute("a422_rel_personid").getValue()[0].name;
var otest = Xrm.Page.getAttribute("a422_rel_testnameid").getValue()[0].name;

Xrm.Page.getAttribute("a422_name").setValue(operson + " - " + otest);
}

/*
function a422_rel_testnameid_onchange()
{
//Set the name of the score record on the create form
if (Xrm.Page.ui.getFormType() == 1)
{
var operson = Xrm.Page.getAttribute("a422_rel_personid").getValue()[0].name;
var otest = Xrm.Page.getAttribute("a422_rel_testnameid").getValue()[0].name;

Xrm.Page.getAttribute("a422_name").setValue(operson + " - " + otest);
}
}
*/