function Form_onload() 
{
    if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() == null)
		{
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
		}	
}

function PhoneForm_onload()
{
	//If phone call is for a person and no phone number is set, use webservices to grab it from the
	//regarding Person.

	var formPhone = Xrm.Page.getAttribute("phonenumber").getValue();
	var regardID = Xrm.Page.getAttribute("to").getValue();

	if ((formPhone === null) && (regardID !== null) && (regardID[0].typename == "contact"))
		{
		SingleValue("contact",regardID[0].id,"telephone2");
		var phoneNum = SingleValueXml.getElementsByTagName("q1:telephone2");

		//Make sure the person has a phone number before trying to grab it by checking the 
		//length of the tag array

		if (phoneNum.length > 0)
			{
			Xrm.Page.getAttribute("phonenumber").setValue(phoneNum[0].text);
			}
		}
}

function Onchange_Activitycategory()
{
	if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() != null)
		{
        Xrm.Page.getAttribute("subject").setValue(null);                                           
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(false);
		}
    else
        {
        Xrm.Page.getAttribute("subject").setValue(null);                                           
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
        }
}

function Onchange_Activityvalue()
{
	if (Xrm.Page.getAttribute("a422_rel_activityvalueid").getValue() != null)
		{
		//******builds the value for the subject for the activitiy
		var osubject = Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue()[0].name + " - " + Xrm.Page.getAttribute("a422_rel_activityvalueid").getValue()[0].name;
		Xrm.Page.getAttribute("subject").setValue(osubject);
		}
	else
		{
		Xrm.Page.getAttribute("subject").setValue(null);
		}
}

function Event_Appointment()
{
  try
  {
    if (window.top.opener && !window.top.opener.closed)
    {
      var parentID = window.top.opener.Xrm.Page.data.entity.getId();
      var parentName = window.top.opener.Xrm.Page.getAttribute('a422_name').getValue();
      var parentEntity = window.top.opener.Xrm.Page.data.entity.getEntityName();
				
    if (parentID !== null && parentName !== null && parentEntity === 'a422_event')
    {
      var lookupData = new Array();
      var lookupItem = new Object();
      lookupItem.id = parentID;
      lookupItem.name = parentName;
      lookupItem.entityType = parentEntity;
      lookupData[0] = lookupItem;
      Xrm.Page.getAttribute('regardingobjectid').setValue(lookupData);
    }
    }
  }
  catch (err)
  {
  }
}