function Mscrm_Isv_a422_opportunityrequirement_HomepageGrid_Group0_Control0_1()
{this.parent.document.all.A422_Opportunity_A422_OpportunityRequirementsFrame.src='https://cgrid.422x.com:4433/OppRequirement.aspx'+window.location.search+'&orgname='+ORG_UNIQUE_NAME;}

function Mscrm_Isv_a422_opportunityrequirement_SubGrid_Group0_Control0_2()
{this.parent.document.all.A422_Opportunity_A422_OpportunityRequirementsFrame.src='https://cgrid.422x.com:4433/OppRequirement.aspx'+window.location.search+'&orgname='+ORG_UNIQUE_NAME;}

//Called from the Edit requirements button on the Opportunity form ribbon
//Creates a div and Iframe in the CRM form area to display the requirements grid
function Form_opportunity_MainTab_Actions_EditReqGrid()
{
var divCheck = document.getElementById('gridDiv');
var topDiv = document.getElementById('tdAreas');

//Removes the grid div if it is already there so they don't stack up.
function removeReqGrid()
{
topDiv.removeChild(divCheck);
}

if (divCheck != null)
{
removeReqGrid();
}

var ourl = "https://cgrid.422x.com:4433/OppRequirement.aspx";
var newdiv = document.createElement('div');
newdiv.setAttribute('id','gridDiv');
newdiv.setAttribute('display','inline');
newdiv.setAttribute('onBlur','removeReqGrid()');
newdiv.innerHTML = '<IFRAME id=editgridIFrame height="100%" width="100%" frameBorder="0" src=' + ourl + window.location.search + '&orgname=' + ORG_UNIQUE_NAME + '></IFRAME>';
topDiv.appendChild(newdiv);
document.getElementById('areaForm').style.display = "none";
}