function Form_onload()
{

/****************Loading address in Bing Map*********************/ 
/*if(Xrm.Page.getAttribute("a422_address").getValue()!=null)
{
var address=Xrm.Page.getAttribute("a422_address").getValue();
//"250 Brent Lane,Pensacola,32523 9160";
crmForm.all.IFRAME_Map.src ="/ISV/AdmissionsLab/TravelManagement/Map.aspx?address="+address;
}*/

/***********  Logic to hide the time fields when All day is ticked ******************/

HideTimeFields=function ()
{
	if(Xrm.Page.getAttribute("a422_allday").getValue()=== true)
	{
		$('.DateTimeUI_RenderTimeControl_td2').hide();
		Xrm.Page.ui.controls.get("a422_enddatetime").setVisible(false);
	}
	else
	{
		$('.DateTimeUI_RenderTimeControl_td2').show();
		Xrm.Page.ui.controls.get("a422_enddatetime").setVisible(true);
	}
}

if(Xrm.Page.getAttribute("a422_allday").getValue() !== null)
{
	HideTimeFields();
}


// Set the Status to Tentative when Event is created.
if(Xrm.Page.ui.getFormType()==1)
{
Xrm.Page.getAttribute("a422_status").setValue('1');
}
//Set the Event Start/End Date to Trip Start Date.

if (Xrm.Page.ui.getFormType() == 1) {

//If form is being opened using an existing parent record
if ((window.opener != null) && (window.opener.parent != null) && (window.opener.parent.document != null) && (window.opener.parent.document.Xrm != null)) 
{
var oParentCrmForm = window.top.opener.parent.Xrm;
if (oParentCrmForm == null || oParentCrmForm == undefined) 
{
}
else 
{
    if (oParentCrmForm.Page.data.entity.getEntityName() == 'a422_trip')
{
var tripStartDate= oParentCrmForm.Page.getAttribute("a422_startdate").getValue();
    Xrm.Page.getAttribute("a422_startdatetime").setValue(tripStartDate);
    Xrm.Page.getAttribute("a422_enddatetime").setValue(tripStartDate);
}
}
}
}

/****************Loading address in Bing Map*********************/ 
if (Xrm.Page.ui.getFormType()== 1)
{
//Hide Map section
Xrm.Page.ui.tabs.get(2).setVisible(false);
}
}

function a422_allday_onchange()
{
	if(Xrm.Page.getAttribute("a422_allday").getValue()==1 && Xrm.Page.getAttribute("a422_startdatetime").getValue() != null)
	{
	var myDate = Xrm.Page.getAttribute("a422_startdatetime").getValue();
	myDate.setDate(myDate.getDate()+1);
	Xrm.Page.getAttribute("a422_enddatetime").setValue(myDate);
	}
HideTimeFields();
}

function sDate_onchange()
{
var sDate = Xrm.Page.getAttribute("a422_startdatetime").getValue();
Xrm.Page.getAttribute("a422_enddatetime").setValue(sDate);
}

function eDate_onchange()
{
	var sDate = Xrm.Page.getAttribute("a422_startdatetime").getValue();
	var eDate = Xrm.Page.getAttribute("a422_enddatetime").getValue();
	if (eDate < sDate)
	{
		alert('End Date and Time must be after Start Date and Time');
		Xrm.Page.getAttribute("a422_enddatetime").setValue(sDate);
	}
}

function a422_organizationid_onchange()
{
//************* On change event to get the address from Organization selected **********//

if(Xrm.Page.getAttribute("a422_organizationid").getValue() !=null)
{
var accountId=Xrm.Page.getAttribute("a422_organizationid").getValue()[0].id;

//Call Web Service to get addresses
var serverUrl = Xrm.Page.context.getServerUrl();
var ODataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";
//Query to get data for related activities
var ODataQuery = ODataPath + "/CustomerAddressSet?$select=Line1,Line3,Line3,City,PostalCode&$filter=(ParentId/Id eq guid'" + accountId + "') and (AddressNumber eq 1)";

$.ajax({
   type: "GET",
   contentType: "application/json; charset=utf-8",
   datatype: "json",
   async: false,
   url: ODataQuery,
   beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
   success: function (data, textStatus, XmlHttpRequest)
	   {
			var results = data.d.results;
				
			//Parse and display the results.
			var address='';

			if(results[0].Line1 != null)
			{
			 address=address.concat(results[0].Line1);
			 address=address.concat(',');
			}

			if(results[0].Line2 !=null)
			{
			 address=address.concat(results[0].Line2);
			 address=address.concat(',');
			}

			if(results[0].Line3 !=null)
			{
			 address=address.concat(results[0].Line3);
			 address=address.concat(',');
			}

			if(results[0].City !=null)
			{
			 address=address.concat(results[0].City);
			 address=address.concat(',');
			}

			if(results[0].PostalCode !=null)
			{
			 address=address.concat(results[0].PostalCode);
			}

			if(address!='' && address != undefined)
			{
			 if (address.lastIndexOf(',') == address.length - 1) 
				  {
						address= address.substring(0, address.length - 1);
					}
			}
			
			Xrm.Page.getAttribute("a422_address").setValue(address);
	   },
	   error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Organization Adress Query Failed: ' + ODataQuery); }
	   });
}
}