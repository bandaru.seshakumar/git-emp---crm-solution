function a422_waived_onchange()
{
//Set the completion date to today's date when a requirement is waived.
//Set it to null when it is "unwaived".

var waive = Xrm.Page.getAttribute("a422_waived").getValue();
var cdate = Xrm.Page.getAttribute("a422_completion").getValue();
var today = new Date();

if (waive == 1) {
cdate = today;
cdate = cdate.setHours(17,0,0);
Xrm.Page.getAttribute("a422_completion").setValue(cdate);
}

if (waive == 0) {
Xrm.Page.getAttribute("a422_completion").setValue(null);
}
}

function Form_onload() 
{
    if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() == null)
		{
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
		}	
}

function Onchange_Activitycategory()
{
	if (Xrm.Page.getAttribute("a422_rel_activitycategoryid").getValue() != null)
		{                                         
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(false);
		}
    else
        {                                          
        Xrm.Page.getAttribute("a422_rel_activityvalueid").setValue(null);                                          
        Xrm.Page.ui.controls.get("a422_rel_activityvalueid").setDisabled(true);
        }
}