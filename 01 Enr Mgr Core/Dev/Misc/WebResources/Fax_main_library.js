function IFRAME_Activities_onload()
{
 
}
function Form_onload()
{
//******Hide lookup fields so only picklists are visible
crmForm.all.a422_rel_activitycategoryid_d.style.display= "none";
crmForm.all.a422_rel_activityvalueid_d.style.display = "none";

//******Extract the orgname from the url for use in script paths.  Adjust accordingly for custom
// client domains.
var query = window.location.href; 
var vars = query.split('/');
var varServer = vars[0] + "/" + "/" + vars[2];
var str1 = vars[2].split('.');
varOrg = str1[0];

//if not create form, strip brackets from category guid before passing to iframe url
try
{
var str1 = crmForm.all.a422_rel_activitycategoryid.DataValue[0].id;
var str2= str1.substr(1);
var crmCatID = str2.substring(0, str2.length-1).toLowerCase();

//Build Iframe path using retrieved varSever variable
crmForm.all.IFRAME_Activities.src = varServer +"/extensions/WebformsCRM/ActivityFrame.aspx?orgname=" + varOrg + "&Category=" + crmCatID;
}
catch(err)
{
//if create form, omit the category guid from iframe url
//Build Iframe path using retrieved varSever variable
crmForm.all.IFRAME_Activities.src = varServer +"/extensions/WebformsCRM/ActivityFrame.aspx?orgname=" + varOrg;
}
}
function Form_onsave()
{

}
