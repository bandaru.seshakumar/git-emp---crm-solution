function Form_onload()
{
//Call functions to change first State and Country fields to lookups and set lookup values from text fields if needed
statePick("stateorprovince");
countryPick("country");
}

function Form_onsave()
{
//set variables for calling web services to generate geomarket
//retrieve only the first 5 digits of zip code
function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}
var ozipcode = Left(Xrm.Page.getAttribute("postalcode").getValue(), 5);

if (Xrm.Page.getAttribute("stateorprovince").getValue() !== null)
{
var ostate = Xrm.Page.getAttribute("stateorprovince").getValue();
}

//Call getGeo function from library to get the geomarket for the set and set it on the form
var geomarket= getGeo(ozipcode, ostate); 
Xrm.Page.getAttribute("a422_geomarket").setValue(geomarket);

//Save State and Country lookup values to text fields using JS library function
saveAdress("stateorprovince", "emStatePick");
saveAdress("country", "emCountryPick");
}

function a422_geomarket_onchange()
{
alert("GeoMarket is calculated from the address automatically when you save the form.\r\nAny changes you make directly to the GeoMarket field will be overwritten!")
}