function CallOppMerge()
{
var PID = Xrm.Page.data.entity.attributes.get("customerid").getValue()[0].id;
var OID = Xrm.Page.data.entity.getId();
var OName = Xrm.Page.data.entity.attributes.get("name").getValue();
var myParams = 'OppID=' + OID + '&PersonID=' + PID + '&OppName=' + OName;
myParams = encodeURIComponent(myParams);
var mURL = '/WebResources/ycg_/html/OppMerge.html?Data=' + myParams;

window.open (mURL,'','top=250,width=800,height=600,status=1,resizable=1,menubar=0,toolbar=0,location=0');
}

//Parse querystring to get record IDs for the grid query
function getDataParam() {
   //Get the any query string parameters and load them
   //into the vals array

   var vals = new Array();
   if (location.search != "") {
    vals = location.search.substr(1).split("&");
    for (var i in vals) {
     vals[i] = vals[i].replace(/\+/g, " ").split("=");
    }
    //look for the parameter named 'data'
    var found = false;
    for (var i in vals) {
     if (vals[i][0].toLowerCase() == "data") {
      var recIDs = new Array();
	  recIDs = decodeURIComponent(vals[i][1]).split("&");
	  return  recIDs
	  break;
     }
    }
    if (!found)
    { alert('Invalid parameters'); }
   }
   else {
    alert('Invalid parameters');
   }
  }

//Get's ID from the selected row/radio button
function getCheckedRadio(name) {
	var radioButtons = document.getElementsByName(name);
	for (var x = 0; x < radioButtons.length; x ++) {
		if (radioButtons[x].checked) {
			return radioButtons[x].value
		}
	}
}

function ShowProgress(OppID,ODataPath,OppName)
{
	//Gets ID for selected parent Opportunity
	var parentOppId = getCheckedRadio('OppSelect');
	
	//If no opportunity is selected alert user, otherwise wait 1 second until processing message loads and kickoff merge
	if (parentOppId)
	{
		//hide container main container div while processing
		document.getElementById('container').style.display = 'none';
		//display loading div while processing
		document.getElementById('loading').style.display = 'block';
		setTimeout(function(){MergeOpp(OppID,ODataPath,OppName,parentOppId);},1000);
	}
	else
	{
		alert('Please select an Opportunity to merge to');
	}
}

//Master function for the merge.  Calls several sub functions.
function MergeOpp(delOppId, ODataPath, oldOppName, parentOppId)
{	
		//Gets Name of Parent Opportunity
		var newOppName = document.getElementById(parentOppId).childNodes[1].innerHTML;
		
		MoveActivities(delOppId, parentOppId, "TaskSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "task");
		MoveActivities(delOppId, parentOppId, "EmailSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "email");
		MoveActivities(delOppId, parentOppId, "ServiceAppointmentSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "serviceappointment");
		MoveActivities(delOppId, parentOppId, "AppointmentSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "appointment");
		MoveActivities(delOppId, parentOppId, "FaxSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "fax");
		MoveActivities(delOppId, parentOppId, "LetterSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "letter");
		MoveActivities(delOppId, parentOppId, "PhoneCallSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "phonecall");
		MoveActivities(delOppId, parentOppId, "RecurringAppointmentMasterSet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "recurringappointment");
		MoveActivities(delOppId, parentOppId, "CampaignActivitySet", "ActivityId", "RegardingObjectId", ODataPath, newOppName, "campaignactivity");
		MoveRecords(delOppId, parentOppId, "A422_InterestSet", "A422_InterestId", "a422_rel_opportunityid", ODataPath, newOppName);
		MoveRecords(delOppId, parentOppId, "AnnotationSet", "AnnotationId", "ObjectId", ODataPath, newOppName);
		MoveRecords(delOppId, parentOppId, "A422_OpportunityStatusChangeSet", "A422_OpportunityStatusChangeId", "a422_opportunityid", ODataPath, newOppName);
		MoveRecords(delOppId, parentOppId, "A422_AwardSet", "A422_AwardId", "a422_rel_opportunityid", ODataPath, newOppName);
		//alert('Records Merged Successfully');
		
		//Create merge task
		GetSysActivity(parentOppId, ODataPath, oldOppName, newOppName);
		
		//Delete Merged Opportunity
		deleteOpp(delOppId, ODataPath);
		
		//Get ID for Requirement checking WF and execute the WF on the parent opportunity
		getWorkflowId('System: Opp Merge Requirement Checking', parentOppId);
		
		//replacer main container content with success message
		document.getElementById('container').innerHTML = "<div class='merge_info' style='text-align: center;'>The Opportunities have been merged successfully." +
			"<br />" +
			"<a href='#' onClick='javascript:window.opener.OppClose(); window.close();'>Close Window</a>" +
			"</div>";
		//hide loading div
		document.getElementById('loading').style.display = 'none';
		//display updated container div
		document.getElementById('container').style.display = 'block';	
		//pWindow.close();
}

//Grabs related records for specified entity and calls the ReParent function to attach records to parent opportunity
function MoveRecords(delOppId, parentOppId, ODataSet, IdField, LinkField, ODataPath, newOppName)
{
	var ODataQuery = ODataPath + "/" + ODataSet + "?$select=" + IdField + "&$filter=" + LinkField + "/Id eq guid'" + delOppId + "'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var rGuid = "";
				
                for( i=0; i< uJson.length; i++)
				{
					rGuid = uJson[i][IdField];
					var uObject = new Object();
					uObject[LinkField] = {Id: parentOppId, LogicalName: "opportunity", Name: newOppName};
					var uRecord = window.JSON.stringify(uObject);
					
					ReParent(uRecord,ODataPath,ODataSet,rGuid);
				}

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('MoveRecords call failed: ' + ODataQuery); }
   });
}

//Grabs related Activity records for specified entity, checks to see if they are open, opens them if they are closed, calls the ReParent function to attach records to parent opportunity, and recloses
//records that were originally closed
function MoveActivities(delOppId, parentOppId, ODataSet, IdField, LinkField, ODataPath, newOppName, ActType)
{
	var ODataQuery = ODataPath + "/" + ODataSet + "?$select=" + IdField + ",StateCode&$filter=" + LinkField + "/Id eq guid'" + delOppId + "'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
	   async: false,
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var uJson = data.d.results;
				var rGuid = "";
				
                for( i=0; i< uJson.length; i++)
				{
					rGuid = uJson[i][IdField];
					var uObject = new Object();
					uObject[LinkField] = {Id: parentOppId, LogicalName: "opportunity", Name: newOppName};
					var uRecord = window.JSON.stringify(uObject);
					
					//for activity entities we need to reopen the activities first, then move them, then close them again
					if (uJson[i].StateCode.Value == 1)
					{
						SetActStatus(rGuid,ActType,0,2);
						ReParent(uRecord,ODataPath,ODataSet,rGuid);
						SetActStatus(rGuid,ActType,1,5);
					}
					else
					{
						ReParent(uRecord,ODataPath,ODataSet,rGuid);
					}
				}

           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('MoveRecords call failed: ' + ODataQuery); }
   });
}

function ReParent(uJson,ODataPath,ODataSet,rGuid)
{
	$.ajax({
	type: "POST",
	contentType: "application/json; charset=utf-8",
	datatype: "json",
	async: false,
	data: uJson,
	url: ODataPath + "/" + ODataSet + "(guid'" + rGuid + "')",
	 
	beforeSend: function (XMLHttpRequest) {
	 
	//Specifying this header ensures that the results will be returned as JSON.
	XMLHttpRequest.setRequestHeader("Accept", "application/json");
	 
	//Specify the HTTP method MERGE to update just the changes you are submitting.
	XMLHttpRequest.setRequestHeader("X-HTTP-Method", "MERGE");
	 
	},
	 
	success: function (data, textStatus, XmlHttpRequest) {
		//alert("Updated successfully");
	},
	 
	error: function (XmlHttpRequest, textStatus, errorThrown) {
		alert("Update Failed.  Please try the operation again or contact support for assistance." + errorThrown);
	}
	});
}

function deleteOpp(recordId, ODataPath) {
 
var deleteRecordReq = new XMLHttpRequest();
 
deleteRecordReq.open('POST', ODataPath + "/OpportunitySet(guid'" + recordId + "')", false);
deleteRecordReq.setRequestHeader("Accept", "application/json");
deleteRecordReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
deleteRecordReq.setRequestHeader("X-HTTP-Method", "DELETE");
deleteRecordReq.send(null);
}

// JScript source code 
function CreateTask(parentOppId, ODataPath, oldOppName, newOppName, CatGuid, ValGuid, UserName) { 
		
	//Build timestamp
    var n = new Date();
	var day = n.getDate();
	var month = n.getMonth() + 1;
	var year = n.getFullYear();
	var hour = n.getHours();
	var min = n.getMinutes();
	var sec = n.getSeconds();
	var dt = month + "/" + day + "/" + year + " " + hour + ":" + min + ":" + sec;
	
	var CRMObject = new Object(); 
    ///////////////////////////////////////////////////////////// 
    // Specify the ODATA entity collection 
    var ODATA_EntityCollection = "/TaskSet"; 
    ///////////////////////////////////////////////////////////// 
    // Define attribute values for the CRM object you want created 
    CRMObject.Subject = "Opportunity Merged: " + oldOppName;
    CRMObject.a422_rel_activitycategoryid = {Id: CatGuid, LogicalName: "a422_dom_activitycategory", Name: "System"};
    CRMObject.a422_rel_activityvalueid = {Id: ValGuid, LogicalName: "a422_dom_activityvalue", Name: "System Action"};
    CRMObject.ActualEnd = dt;
    CRMObject.Description = "This Opportunity was merged with:\n\nOpportunity: " + oldOppName + "\n\nBy: " + UserName;
    CRMObject.RegardingObjectId = {Id: parentOppId, LogicalName: "opportunity", Name: newOppName};
    //Parse the entity object into JSON 
    var jsonEntity = window.JSON.stringify(CRMObject); 
    //Asynchronous AJAX function to Create a CRM record using OData 
    $.ajax({ type: "POST", 
        contentType: "application/json; charset=utf-8", 
        datatype: "json", 
        url: ODataPath + ODATA_EntityCollection, 
        data: jsonEntity, 
        beforeSend: function (XMLHttpRequest) { 
            //Specifying this header ensures that the results will be returned as JSON. 
            XMLHttpRequest.setRequestHeader("Accept", "application/json"); 
        }, 
        success: function (data, textStatus, XmlHttpRequest) {  
            var NewCRMRecordCreated = data["d"]; 
            //alert("CRM GUID created: " + NewCRMRecordCreated.ActivityId); 
			//close task with soap call
			SetActStatus(NewCRMRecordCreated.ActivityId,'task',1,5);
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert("Task creation failed"); 
        } 
    }); 
}

function GetSysActivity(parentOppId, ODataPath, oldOppName, newOppName)
{
	var ODataQuery = ODataPath + "/A422_dom_activityvalueSet?$select=A422_dom_activityvalueId,a422_rel_activitycategoryid&$filter=A422_ActivityValue eq 'System Action'";
	
	$.ajax({
       type: "GET",
       contentType: "application/json; charset=utf-8",
       datatype: "json",
       url: ODataQuery,
       beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
       success: function (data, textStatus, XmlHttpRequest)
           {
				var myData = data.d.results;
				
				//Get guids for Activity values needed for task creation
				var CatGuid = myData[0].a422_rel_activitycategoryid.Id;
				var ValGuid = myData[0].A422_dom_activityvalueId;
				loggedUser(parentOppId, ODataPath, oldOppName, newOppName, CatGuid, ValGuid);
           },
       error: function (XmlHttpRequest, textStatus, errorThrown) { document.write('Error retrieving Activity Values for Task creation: ' + ODataQuery); }
   });
}

function SetActStatus(ActID,ActType,ActState,ActStatus) 
{         
	// Once the task is created we need to close it using a soap call
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
	request += "<s:Body>";    
	request += "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    request += "<request i:type=\"b:SetStateRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
    request += "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    request += "<a:KeyValuePairOfstringanyType>";    
	request += "<c:key>EntityMoniker</c:key>";    
	request += "<c:value i:type=\"a:EntityReference\">";
    request += "<a:Id>" + ActID + "</a:Id>";
    request += "<a:LogicalName>" + ActType + "</a:LogicalName>";    
	request += "<a:Name i:nil=\"true\" />";    
	request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";    
	request += "<a:KeyValuePairOfstringanyType>";    
	request += "<c:key>State</c:key>";    
	request += "<c:value i:type=\"a:OptionSetValue\">";    
	request += "<a:Value>" + ActState + "</a:Value>";  //State***********************************************
	request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";    
	request += "<a:KeyValuePairOfstringanyType>";    
	request += "<c:key>Status</c:key>";    
	request += "<c:value i:type=\"a:OptionSetValue\">";    
	request += "<a:Value>" + ActStatus + "</a:Value>"; //Status****************************************************
	request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";    
	request += "</a:Parameters>";
    request += "<a:RequestId i:nil=\"true\" />";    
	request += "<a:RequestName>SetState</a:RequestName>";    
	request += "</request>";    
	request += "</Execute>";    
	request += "</s:Body>";    
	request += "</s:Envelope>";   
	
	//send set state request    
	$.ajax({        type: "POST",
        contentType: "text/xml; charset=utf-8",        datatype: "xml",
        url: Xrm.Page.context.getServerUrl() + "/XRMServices/2011/Organization.svc/web",
        data: request,
		async: false,
		beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/xml, text/xml, */*");
            XMLHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
        },        
		success: function (data, textStatus, XmlHttpRequest) {
            //alert('Task Closed');        
			},
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);        
			}    
		});    
	}
	
function loggedUser(parentOppId, ODataPath, oldOppName, newOppName, CatGuid, ValGuid) {
	 //Get GUID of logged user
	 var user = Xrm.Page.context.getUserId(); 
	 var userId = user.substring(1,37);
	
	 var ODATA_EntityCollection = "/SystemUserSet";
	 // Specify the ODATA Query
	 var ODATA_Query = "(guid\'" + userId + "')";
	 // Combine into the final URL
	 var ODATA_Final_url = ODataPath + ODATA_EntityCollection + ODATA_Query;

	//Calls the REST endpoint to retrieve data
	 $.ajax({
		 type: "GET",
		 contentType: "application/json; charset=utf-8",
		 datatype: "json",
		 url: ODATA_Final_url,
		 beforeSend: function (XMLHttpRequest) {
			 XMLHttpRequest.setRequestHeader("Accept", "application/json");
		 },
		 success: function (data, textStatus, XmlHttpRequest) {
			var UserName = data.d.FullName;
			CreateTask(parentOppId, ODataPath, oldOppName, newOppName, CatGuid, ValGuid, UserName);
			},
		 error: function (XmlHttpRequest, textStatus, errorThrown) {
			 alert('Error: '+ ODATA_Final_url);

			}
	 });

}

function ExecuteWF(oppID, wfID) {

/*Generate Soap Body.*/
var soapBody = "<soap:Body>" +
		 "  <Execute xmlns='http://schemas.microsoft.com/crm/2007/WebServices'>" +
		 "    <Request xsi:type=\'ExecuteWorkflowRequest\'>" +
		 "      <EntityId>" + oppID + "</EntityId>" +
		 "      <WorkflowId>" + wfID + "</WorkflowId>" +
		 "    </Request>" +
		 "  </Execute>" +
		 "</soap:Body>";

/*Wrap the Soap Body in a soap:Envelope.*/
var soapXml = "<soap:Envelope " +
		"  xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' " +
		"  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
		"  xmlns:xsd='http://www.w3.org/2001/XMLSchema'>" +
		GenerateAuthenticationHeader() +
		soapBody +
		"</soap:Envelope>";

/* Create the XMLHTTP object for the execute method.*/
var serverUrl = Xrm.Page.context.getServerUrl() + "/MSCRMservices/2007/crmservice.asmx";
//var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
var xmlhttp = new XMLHttpRequest();
xmlhttp.open("POST", serverUrl, false);
xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
xmlhttp.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/crm/2007/WebServices/Execute");

/* Send the XMLHTTP object. */
xmlhttp.send(soapXml);
}

function getWorkflowId(workflowName, oppID) {
    var odataSelect = Xrm.Page.context.getServerUrl() + '/XRMServices/2011/OrganizationData.svc/WorkflowSet?$select=WorkflowId&$filter=StateCode/Value eq 1 and ParentWorkflowId/Id eq null and Name eq \'' + workflowName + '\'';

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", odataSelect, false);
    xmlHttp.send();

    if (xmlHttp.status == 200) {
        var result = xmlHttp.responseXML;
		xmlDoc = result;
        
        var wfID = xmlDoc.getElementsByTagName("d:WorkflowId")[0].childNodes[0].nodeValue;
		ExecuteWF(oppID, wfID)
    }
}

